import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
// import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { ICON_URL } from '../../constants/config';
import { degToCompassDirection, testModLog } from '../../libs/lib';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    textAlign: 'initial',
    margin: 10,
    display: 'flex',
    flex: '1 1',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

const Day = ({
    temperatureUnit = '°С',

    wind = '',
    visibility = '',
    
    city = '',
    date = '',
    pressure = '',
    humidity = '',
    temperatureCur = '',
    // temperatureMin = '',
    // temperatureMax = '',
    weather = '',
    

}) => {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;
    
    const ye = new Intl.DateTimeFormat( 'en', { year: 'numeric' } ).format( date );
    const mo = new Intl.DateTimeFormat( 'en', { month: 'short' } ).format( date );
    const da = new Intl.DateTimeFormat( 'en', { day: '2-digit' } ).format( date );
    testModLog(`${da}-${mo}-${ye}`);

    const imgSrc = weather 
        ? ICON_URL.replace( '__WEATHER_ICON__', weather[0].icon ) 
        : '';
    
        const imgDescription = weather 
    ?  weather[0].description 
    : '';

    return (
        <Card className={ classes.root } variant="outlined">
            <CardContent>
            <Typography className={ classes.title } color="textSecondary" gutterBottom >
                Day: - {`${da}-${mo}-${ye}`} at { city } 
                { weather && <img src={ imgSrc } alt={ imgDescription } /> }
            </Typography>

            <Typography variant="h5" component="h5">
                { bull } { temperatureCur } { temperatureUnit }, { imgDescription }
            </Typography>

            <Typography variant="h5" component="h5">
                { bull } wind: { wind.speed } { degToCompassDirection( wind.direction ) }
            </Typography>

            <Typography variant="h5" component="h5">
                { bull } pressure: { pressure } hPa
            </Typography>

            <Typography variant="h5" component="h5">
                { bull } Humidity: { humidity }%
            </Typography>


            <Typography variant="h5" component="h5">
                { bull } Visibility: { visibility }m
            </Typography>

            <Typography variant="body2" component="p">
            more functions at release version
            <br />
            </Typography>
        </CardContent>
        <CardActions>
            {/* <Button size="small">Learn More</Button> // for future version */}
        </CardActions>
        </Card>
    );
};

export default Day;