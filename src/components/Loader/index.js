import React, { useEffect } from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import { makeStyles } from '@material-ui/core/styles';
import { TEST_MOD, TEST_MOD_LOADING_TIME } from '../../constants/config';

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

export default function Loader( { show, clickToHide = false } ) {
  const classes = useStyles();
  const [open, setOpen] = React.useState( show );

  useEffect( () => {
    if ( TEST_MOD ) {
      setTimeout(() => {
        
        setOpen( show );
      }, TEST_MOD_LOADING_TIME );
    } else setOpen( show );
  },
    [ show ]
  );

  const handleClose = () => {
    if ( !clickToHide ) return;
    setOpen(false);
  };
  

  return (
    <div>
      <Backdrop className={classes.backdrop} open={open} onClick={ handleClose }>
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
}
