import React, { useState, useEffect } from 'react';

import { Container, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

import { LINK, WITH_COUNTRY, TEST_MOD, TEST_MOD_DEFAULT_CITY, TEST_LET_DETECT_CURRENT_CITY } from '../../constants/config';
import Day from '../Day';
import dataForTest, { CitiesArr } from '../../constants/dataForTest';
import Loader from '../Loader';
import { testModLog } from '../../libs/lib';



const useStyles = makeStyles(  ( theme ) => ( {
    formWrapper: {
        padding: '20px 0',
    },
    daysRowWrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: '1 1 100%',
        justifyContent: 'space-around',

    },
    errorsBlockWrapper: {
        padding: '20px',
        color: 'red',
        fontSize: '20px',
        fontWeight: 700,
    },
    form: {
        '& > *': {
            margin: theme.spacing( 1 ),
            '& > buttonOk': {
                margin: theme.spacing( 1 ),
                height: '1.1876em',
                '& >*': {
                    lineHeight: 2.5,
                    fontSize: 18,
                    color: 'red'
                },
                padding: '18.5px 14px',
            },
        },
    },
} )  );


export const WeatherBox = () => {
    
    const classes = useStyles();
    
    const [country, setCountry] = useState(''); // for future version

    const [city, setCity] = useState('');
    const [inputCityValue, setInputCityValue] = React.useState('');
    
    const [weatherData, setWeatherData] = useState({});
    const [error, setError] = useState();
    const [loading, setLoading] = useState( true ); // FALSE! True - only for test(!).

    const errorEnterTheCity = "Enter the city name!";
    const errorNotAllowedAccessToClientImposition = "Not allowed access to the use of the used imposition!";

    
    const getCity = async () => {

        
        setError( undefined );
        setLoading( true );
        const link = TEST_LET_DETECT_CURRENT_CITY 
            ? "http://ip-api.com/json/" // origin
            : "http://ip-api.com/json/af"; // for test at ERROR

        const api_call = await fetch( link ); 
        let res;
        testModLog( 'getCity -- api_call:', api_call );
        const data = await api_call.json();
        
        if ( data && data.status === "fail" ) {
            testModLog( 'getCity -- data && data.status === "fail":', errorNotAllowedAccessToClientImposition );
            setError( errorNotAllowedAccessToClientImposition );
            res = await '';
        } else res = data.regionName;

        testModLog( 'getCity -- data:', data );
        debugger;
        
        setError( undefined );
        setLoading( false );
        return res;
    };

    const getData = async () => {
        if ( ! city ) {
            console.error( "getData -- city !city:", city );
            
            setError( errorEnterTheCity );
            return;
        }
        const cLink = normalizeLink( LINK );
        testModLog( 'getData - normalized Link:', cLink );

        setError( undefined );
        setLoading( true );

        if ( TEST_MOD ) {
            testModLog( 'test mod start' );
            let dataJson = dataForTest;
            testModLog( 'getData - test mod dataJson:', dataJson );
            setWeatherData( dataJson );
                
            setError( false );
            setLoading( false );
            return;
        }

        fetch( cLink )
        .then( async data => {
            testModLog( 'getData - Response data:', data );
            let dataJson = await data.json()
            testModLog( 'getData - Response dadataJsona:', dataJson );

            if ( data.status !== 200 ) {
                /* data
                    ok: false
                    redirected: false
                    status: 404
                    statusText: "Not Found"
                */
                console.error( 'getData error:', dataJson.message ) 
                setError( dataJson.cod + ' - ' + dataJson.message );
                setLoading( false );
                setWeatherData( false )
                return false;
            }

            setWeatherData( dataJson );
            
            
            setError( false );
            setLoading( false );
            return dataJson;

        } )
        .catch(  err => {
            console.error( 'getData error:', err ) 
            setError( err.message );
            setLoading( false );
        } );

        
    };

    useEffect( async () => {
        
        let c = await getCity();
        testModLog( 'getCity city:', c );
        
        let cCity = TEST_MOD ? TEST_MOD_DEFAULT_CITY : c;
        if ( cCity ) {
            setCity( cCity );
        } else {
            setError( errorNotAllowedAccessToClientImposition );
        }
    }, [  ] );

    useEffect( () => {
        if ( !!city ) getData();
    }, [ city ] );

    const normalizeLink = ( link ) => {
        let reCountry = /,__COUNTRY__/gi;
        let reCity = /__CITY__/gi;

        let cCity = TEST_MOD ? TEST_MOD_DEFAULT_CITY : city; 
        let cCountry = WITH_COUNTRY ? `,${country}` : '';
        let cLink = link.replace( reCountry, cCountry ).replace( reCity, cCity );
        testModLog( 'normalizeLink cCity:', cCity );
        

        testModLog( 'normalizeLink cLink:', cLink );
        
        return cLink;
    };

    const onCityChange = () => {
        testModLog( 'onCityChange start' );

        let cityValue = inputCityValue;
        testModLog( 'onCityChange -- city:', city, 'cityValue:', cityValue );

        if ( ! cityValue ) { setError( errorEnterTheCity ); return; }
        setCity( cityValue );
    };

    const submitHandler = () => {
        testModLog( 'submitHandler start' );
        
        onCityChange( );
    };

    const renderDay = ( day, dIndex ) => {
        const { dt: date, wind, visibility, } = day;
        const { 
            humidity,
            pressure,
            temp,
            temp_max,
            temp_min, 
        } = day.main;

        return  <Day
            key={ dIndex + '' + day }
            city={ weatherData && weatherData.city.name }
            date={ date * 1000 }
            pressure={ pressure }
            humidity= { humidity }
            temperatureCur={ temp }
            temperatureMin={ temp_min }
            temperatureMax={ temp_max }
            wind={ wind }
            visibility={ visibility }
            weather={ day.weather }
        />;
    };

    const renderRowOfDays = ( ) => {
        // сменить стиль строки - читабельность слабая.
        // take only one string per day.
        let list = weatherData.list.filter(
            ( day, dIndex ) => {
                // if its first item - take it
                return dIndex === 0 
                    ? true 
                    // else - need compare with prev item as Date -- ( dt_txt.split( ' ' )[0] )
                    : weatherData.list[dIndex-1] 
                        && day.dt_txt.split( ' ' )[0] !== weatherData.list[dIndex-1].dt_txt.split( ' ' )[0] 
                }
        );
        debugger
        return list.map(  ( day, dIndex ) => renderDay( day, dIndex )  );
    };


    return (
        <div>
            <Container 
            // maxWidth="sm"
            >
                <div className={ classes.formWrapper }>
                    <form 
                        className={ classes.form } 
                        noValidate 
                        autoComplete="off"
                        onSubmit={ e => { e.preventDefault(); submitHandler() } }
                    >
                        <Autocomplete
                            id="city"
                            freeSolo
                            options={ CitiesArr }
                            
                            value={ city }
                            onChange={  ( event, newValue ) => {
                                setCity( newValue );
                            }}

                            
                            inputValue={inputCityValue}
                            onInputChange={  ( event, newInputValue ) => {
                                setInputCityValue( newInputValue );
                            console.log( 'inputValue change', newInputValue );
                            }  }

                            renderInput={  ( params ) => (
                                <TextField { ...params } label="search city" margin="normal" variant="outlined" />
                            )  }
                        />

                        <Button variant="outlined" size="large" color="primary" className={ 'buttonOk' } onClick={ submitHandler } >Ok</Button>
                    </form>
                </div> 
                <div className={ classes.errorsBlockWrapper }>
                    { error && <div className="error-block"> { error } </div> }
                </div>
                <div className={ classes.daysRowWrapper }>
                    { !loading && weatherData && weatherData.list && renderRowOfDays() }
                </div>
                <Loader show={ loading } clickToHide={ false } />
            </Container>
        </div>
    )
}
