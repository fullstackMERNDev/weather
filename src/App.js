import React, { Component } from 'react';
import './App.css';
import { WeatherBox } from './components/WeatherBox';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Test work for yvision.kz</h1>
          <h2>Тестовое задание:</h2>

          <p>
            Напишите простое SPA которое будет общаться с любым открытым погодным API, и выводить прогноз на сегодня и на ближайшую неделю.
            Нужно детектить текущий город и давай возможность выбрать любой другой из списка с фильтрацией. 
            Приложение должно выглядеть современно и приятно.
            Не забудьте про обработку ошибок :)
          </p>

          <p>Бонус за покрытие тестами.</p>

          <p>Все технологии кроме React на ваш выбор</p>
        </header>
        <WeatherBox />
      </div>
    );
  }
}

export default App;
