const API_KEY = "65c5efd3f6f820cb6f32e29ad7c8cc32";
const UNITS = "metric";
const WITH_COUNTRY = false;
const LANG = 'ru';

// const cnt = 5; // for $$ version API_KEY

const TEST_MOD = false;
const TEST_LOG = true;
const TEST_MOD_LOADING_TIME = 3000;
const TEST_MOD_DEFAULT_CITY = 'TestCity';
const TEST_LET_DETECT_CURRENT_CITY = false;


// link example
// api.openweathermap.org/data/2.5/weather?q=Kiev,ua&appid=ВАШ_КЛЮЧ&units=metric

// for one day
// const link = `http://api.openweathermap.org/data/2.5/weather?q=__CITY__,__COUNTRY__&appid=${API_KEY}&units=${UNITS}&lang=${LANG}`;

//for forecast
// const link = `http://api.openweathermap.org/data/2.5/forecast/daily?q=__CITY__&units=${UNITS}&lang=${LANG}&appid=${API_KEY}`;
const LINK = `http://api.openweathermap.org/data/2.5/forecast?q=__CITY__&units=${UNITS}&lang=${LANG}&appid=${API_KEY}`;
const ICON_URL = "http://openweathermap.org/img/w/__WEATHER_ICON__.png";



export { 
    API_KEY,
    LANG,
    UNITS,
    LINK,
    WITH_COUNTRY,
    TEST_MOD,
    TEST_LOG,
    TEST_MOD_LOADING_TIME,
    TEST_MOD_DEFAULT_CITY,
    TEST_LET_DETECT_CURRENT_CITY,
    ICON_URL,
};